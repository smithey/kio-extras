# translation of kio_fish.po to Italian
# Copyright (C) 2003, 2004, 2009 Free Software Foundation, Inc.
#
# Andrea Rizzi <rizzi@kde.org>, 2003.
# Andrea RIZZI <arizzi@pi.infn.it>, 2004.
# Nicola Ruggero <nicola@nxnt.org>, 2009, 2013.
# Federico Zenith <federico.zenith@member.fsf.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-21 00:46+0000\n"
"PO-Revision-Date: 2013-01-30 17:24+0100\n"
"Last-Translator: Nicola Ruggero <nicola@nxnt.org>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: fish.cpp:291
#, kde-format
msgid "Connecting..."
msgstr "Connessione..."

#: fish.cpp:593
#, kde-format
msgid "Initiating protocol..."
msgstr "Inizializzazione protocollo..."

#: fish.cpp:629
#, kde-format
msgid "Local Login"
msgstr "Accesso locale"

#: fish.cpp:631
#, kde-format
msgid "SSH Authentication"
msgstr "Autenticazione SSH"

#: fish.cpp:670 fish.cpp:690
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:670 fish.cpp:690
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:771
#, kde-format
msgid "Disconnected."
msgstr "Disconnesso."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Nicola Ruggero"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "nicola@nxnt.org"
