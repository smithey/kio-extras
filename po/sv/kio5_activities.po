# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-24 00:16+0000\n"
"PO-Revision-Date: 2016-03-09 17:33+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: fileitemplugin/FileItemLinkingPlugin.cpp:96 KioActivities.cpp:301
#, kde-format
msgid "Activities"
msgstr "Aktiviteter"

#: fileitemplugin/FileItemLinkingPlugin.cpp:99
#, kde-format
msgid "Loading..."
msgstr "Läser in..."

#: fileitemplugin/FileItemLinkingPlugin.cpp:118
#, kde-format
msgid "The Activity Manager is not running"
msgstr "Aktivitetshanteraren kör inte"

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:53
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:125
#, kde-format
msgid "Link to the current activity"
msgstr "Länka till aktuell aktivitet"

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:56
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:130
#, kde-format
msgid "Unlink from the current activity"
msgstr "Ta bort länk för aktuell aktivitet"

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:59
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:134
#, kde-format
msgid "Link to:"
msgstr "Länka till:"

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:64
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:142
#, kde-format
msgid "Unlink from:"
msgstr "Ta bort länk från:"

#: KioActivities.cpp:101 KioActivities.cpp:224
#, kde-format
msgid "Activity"
msgstr "Aktivitet"

#: KioActivities.cpp:223
#, kde-format
msgid "Current activity"
msgstr "Nuvarande aktivitet"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Default"
#~ msgstr "Förval"

#~ msgctxt "@action"
#~ msgid "Switch to activity \"%1\""
#~ msgstr "Byt till aktivitet \"%1\""
